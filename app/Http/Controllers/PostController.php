<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use App\Models\Filters\PostFilter;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostCollection;

class PostController extends Controller
{
    public function index(PostFilter $filter, Request $request)
    {
        $posts = Post::with(['category', 'user', 'tags', 'favoriteUsers'])
            ->withCount(['comments', 'favoriteUsers'])
            ->filter($filter)
            ->paginate((int) $request->input('per_page', 10));

        return new PostCollection($posts);
    }

    public function show(Post $post)
    {
        return new PostResource($post);
    }

    public function showByKeys($username, $slug)
    {
        $user = User::where('name', $username)->firstOrFail();
        $post = Post::where('user_id', $user->id)->where('slug', $slug)->firstOrFail();

        return new PostResource($post);
    }

    public function store(Request $request, Post $post)
    {
        $post->fill($request->all());
        $post->user()->associate(auth()->user());
        $post->save();

        $post->tags()->sync($request->tag_ids);

        return new PostResource($post);
    }

    public function update(Request $request, Post $post)
    {
        $this->authorize('update', $post);

        $post->fill($request->all())->save();
        $post->tags()->sync($request->tag_ids);

        return new PostResource($post);
    }

    public function destroy($id)
    {
        $post = Post::find($id);

        if (! is_null($post)) {
            $this->authorize('delete', $post);
            $post->delete();
        }

        return response()->json();
    }
}
