<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Http\Resources\PostResource;

class FavoriteController extends Controller
{
    public function store(Request $request, Post $post)
    {
        $post->favoriteBy(auth()->user());
        return new PostResource($post);
    }

    public function destroy(Post $post)
    {
        $post->unfavoriteBy(auth()->user());
        return new PostResource($post);
    }
}
