<?php

namespace App\Http\Controllers\Auth;

trait SendTokenAndUser
{
    /**
     * Send the response with the token and the user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResponseWithTokenAndUser()
    {
        return response()->json([
            'access_token' => (string) $this->guard()->getToken(),
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
            'user' => $this->guard()->user()
        ]);
    }
}
