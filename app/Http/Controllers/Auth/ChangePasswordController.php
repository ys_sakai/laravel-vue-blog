<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Controller;

class ChangePasswordController extends Controller
{
    /**
     * Update the user's password.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validateChangePassword($request);

        $this->guard()->user()->update([
            'password' => bcrypt($request->new_password),
        ]);
    }

    /**
     * Validate the change password request.
     *
     * @param Request $request
     * @return void
     */
    protected function validateChangePassword(Request $request)
    {
        Validator::make($request->all(), [
            'password' => 'required|string',
            'new_password' => 'required|string|min:6|confirmed'
        ])->setAttributeNames([
            'password' => '旧パスワード',
            'new_password' => '新パスワード'
        ])->validate();

        if (! Hash::check($request->password, $this->guard()->user()->password)) {
            throw ValidationException::withMessages([
                'password' => '旧パスワードが正しくありません。'
            ]);
        }
    }

    /**
     * Get the guard to be used during change.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
