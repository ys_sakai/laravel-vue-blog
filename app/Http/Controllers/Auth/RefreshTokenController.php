<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;

class RefreshTokenController extends Controller
{
    use SendTokenAndUser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a refresh token request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        try {
            $token = $this->guard()->refresh();
        } catch (JWTException $e) {
            throw new AuthenticationException($e->getMessage());
        }

        $this->guard()->setToken($token);

        return $this->sendResponseWithTokenAndUser();
    }

    /**
     * Get the guard to be used during refresh.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
