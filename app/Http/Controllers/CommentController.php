<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function index()
    {
        return Comment::with('user')->get();
    }

    public function show(Comment $comment)
    {
        return $comment->load('user');
    }

    public function store(Request $request, Comment $comment)
    {
        $comment->fill($request->all());
        $comment->user()->associate(auth()->user());
        $comment->save();

        return $comment->load('user');
    }

    public function update(Request $request, Comment $comment)
    {
        $this->authorize('update', $comment);

        $comment->fill($request->all())->save();

        return $comment->load('user');
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);

        if (! is_null($comment)) {
            $this->authorize('delete', $comment);
            $comment->delete();
        }

        return response()->json();
    }
}
