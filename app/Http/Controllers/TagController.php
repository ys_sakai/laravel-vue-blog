<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;

class TagController extends Controller
{
    public function index()
    {
        return Tag::withCount('posts')->get();
    }

    public function show(Tag $tag)
    {
        return $tag;
    }

    public function store(Request $request, Tag $tag)
    {
        $tag->fill($request->all())->save();
        return $tag;
    }

    public function update(Request $request, Tag $tag)
    {
        $tag->fill($request->all())->save();
        return $tag;
    }

    public function destroy($id)
    {
        Tag::destroy($id);
        return response()->json();
    }
}
