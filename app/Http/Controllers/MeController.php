<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserProfileRequest;

class MeController extends Controller
{
    public function show()
    {
        return auth()->user();
    }

    public function update(UserProfileRequest $request)
    {
        auth()->user()->fill($request->only('email', 'bio'))->save();

        return auth()->user();
    }
}
