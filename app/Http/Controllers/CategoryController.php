<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::withCount('posts')->get();
    }

    public function show(Category $category)
    {
        return $category;
    }

    public function store(Request $request, Category $category)
    {
        $category->fill($request->all())->save();
        return $category;
    }

    public function update(Request $request, Category $category)
    {
        $category->fill($request->all())->save();
        return $category;
    }

    public function destroy($id)
    {
        Category::destroy($id);
        return response()->json();
    }
}
