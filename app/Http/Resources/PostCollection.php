<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCollection extends ResourceCollection
{
    /**
     * コレクションリソースを配列へ変換する
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($post) {
            return $this->toEachArray($post);
        })->all();
    }

    protected function toEachArray($post)
    {
        return [
            'id' => $post->id,
            'category_id' => $post->category_id,
            'user_id' => $post->user_id,
            'slug' => $post->slug,
            'title' => $post->title,
            'content' => $post->content,
            'created_at' => (string) $post->created_at,
            'updated_at' => (string) $post->updated_at,

            'category' => $post->category,
            'user' => $post->user,
            'tags' => $post->tags,
            'comments_count' => $post->comments_count,
            'favorite_users_count' => $post->favorite_users_count,
            'is_favorited' => $post->is_favorited
        ];
    }
}
