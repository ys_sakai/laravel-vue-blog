<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PostResource extends Resource
{
    /**
     * リソースを配列へ変換する
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'user_id' => $this->user_id,
            'slug' => $this->slug,
            'title' => $this->title,
            'content' => $this->content,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,

            'category' => $this->category,
            'user' => $this->user,
            'tags' => $this->tags,
            'comments' => $this->comments->load('user'),
            'favorite_users_count' => $this->favorite_users_count ?? $this->favoriteUsers()->count(),
            'is_favorited' => $this->is_favorited
        ];
    }
}
