<?php

namespace App\Models\Filters;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

abstract class Filter
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Builder
     */
    protected $query;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * フィルタを適用する
     *
     * @param Builder $query
     * @return Builder
     */
    public function apply(Builder $query)
    {
        $this->query = $query;

        $this->executeJoin();

        $inputs = $this->request->all();

        foreach ($inputs as $key => $value) {
            if ($key === 'sort') {
                $this->applySort($value);
            } else {
                $this->applyFilter($key, $value);
            }
        }

        return $this->query;
    }

    /**
     * JOINを実行する
     *
     * フィルタやソートで使用するテーブルをJOINするために使用する。
     * プレフィックスshouldJoinが付与された判定メソッドがtrueの場合、
     * プレフィックスjoinが付与されたJOINメソッドを実行する。
     */
    protected function executeJoin()
    {
        $methods = $this->getJoinMethods();

        foreach ($methods as $method) {
            $shouldMethod = 'should'.Str::studly($method);

            if (method_exists($this, $shouldMethod) && ! $this->$shouldMethod()) {
                continue;
            }

            $this->$method();
        }
    }

    /**
     * JOINメソッドを取得する
     *
     * @return \Illuminate\Support\Collection
     */
    protected function getJoinMethods()
    {
        return collect(get_class_methods($this))->filter(function ($method) {
            return Str::startsWith($method, 'join');
        });
    }

    /**
     * 通常のフィルタを適用する
     *
     * キーにプレフィックスfilterを付与したメソッドを実行する
     *
     * @param string $key
     * @param mixed $value
     */
    protected function applyFilter($key, $value)
    {
        $method = 'filter'.Str::studly($key);

        if (method_exists($this, $method)) {
            if ($value) {
                $this->$method($value);
            } else {
                $this->$method();
            }
        }
    }

    /**
     * ソートを適用する
     *
     * ソート名にプレフィックスsortが付与されたメソッドを実行する
     *
     * @param string $sort
     * @return void
     */
    protected function applySort($sort)
    {
        $method = 'sort'.Str::studly($sort);

        if (method_exists($this, $method)) {
            $this->$method();
        }
    }

    /**
     * 作成時間の降順でソートする
     */
    protected function sortLatest()
    {
        $this->query->latest();
    }

    /**
     * 作成時間の昇順でソートする
     */
    protected function sortOldest()
    {
        $this->query->oldest();
    }
}
