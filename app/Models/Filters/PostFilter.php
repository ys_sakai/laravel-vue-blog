<?php

namespace App\Models\Filters;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Favorite;

class PostFilter extends Filter
{
    /**
     * ユーザのお気に入りをJOINするか判定する
     *
     * @return bool
     */
    protected function shouldJoinUserFavorites()
    {
        return $this->request->has('favorite_username');
    }

    /**
     * 指定のユーザ(favorite_username)のお気に入りをJOINする
     */
    protected function joinUserFavorites()
    {
        $username = $this->request->input('favorite_username');

        $user = User::where('name', $username)->first();
        $userId = $user ? $user->id : null;

        $subQuery = Favorite::select('post_id', 'created_at')->where('user_id', $userId);

        $raw = DB::raw("({$subQuery->toSql()}) as user_favorites");

        $this->query->join($raw, 'user_favorites.post_id', '=', 'posts.id')
            ->mergeBindings($subQuery->getQuery());
    }

    /**
     * 投稿ごとの最新の作成時間のお気に入りをJOINするか判定する
     *
     * @return bool
     */
    protected function shouldJoinLatestFavorites()
    {
        return $this->request->input('sort') === 'favorites_latest'
            && ! $this->shouldJoinUserFavorites();
    }

    /**
     * 投稿ごとの最新の作成時間のお気に入りをJOINする
     */
    protected function joinLatestFavorites()
    {
        $subQuery = Favorite::select('post_id', DB::raw('max(created_at) as max_created_at'))
                                ->groupBy('post_id');

        $raw = DB::raw("({$subQuery->toSql()}) as latest_favorites");

        $this->query->leftJoin($raw, 'latest_favorites.post_id', '=', 'posts.id');
    }

    /**
     * ユーザ名でフィルタリングする
     *
     * @param string $username
     */
    protected function filterUsername($username)
    {
        $user = User::where('name', $username)->first();
        $userId = $user ? $user->id : null;

        $this->query->where('user_id', $userId);
    }

    /**
     * カテゴリーでフィルタリングする
     *
     * @param string $slug
     */
    protected function filterCategorySlug($slug)
    {
        $category = Category::where('slug', $slug)->first();
        $categoryId =$category ? $category->id : null;

        $this->query->where('category_id', $categoryId);
    }

    /**
     * タグでフィルタリングする
     *
     * @param string $slug
     */
    protected function filterTagSlug($slug)
    {
        $tag = Tag::where('slug', $slug)->first();
        $postIds = $tag ? $tag->posts()->pluck('post_id')->toArray() : [];

        $this->query->whereIn('id', $postIds);
    }

    /**
     * お気に入りにしているユーザ名でフィルタリングする
     *
     * @param string $username
     */
    protected function filterFavoriteUsername($username)
    {
        // PostFilter#joinUserFavoritesにて内部結合をすることでフィルタリングを行うため、
        // ここでは何もしない
    }

    /**
     * お気に入り数の降順でソートする
     */
    protected function sortFavoritesCount()
    {
        $this->query->orderBy('favorite_users_count', 'desc');
    }

    /**
     * お気に入りの作成時間の降順でソートする
     */
    protected function sortFavoritesLatest()
    {
        if ($this->shouldJoinUserFavorites()) {
            $this->query->orderByRaw('user_favorites.created_at desc nulls last');
        } else {
            $this->query->orderByRaw('latest_favorites.max_created_at desc nulls last');
        }
    }
}
