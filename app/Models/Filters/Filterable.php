<?php

namespace App\Models\Filters;

trait Filterable
{
    /**
     * フィルタを適用するクエリスコープ
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Filter $filter
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, Filter $filter)
    {
        return $filter->apply($query);
    }
}
