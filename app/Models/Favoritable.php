<?php

namespace App\Models;

trait Favoritable
{
    /**
     * お気に入りにしているユーザを取得する
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function favoriteUsers()
    {
        return $this->belongsToMany(User::class, 'favorites', 'post_id', 'user_id')->withTimestamps();
    }

    /**
     * ログインユーザにお気に入りにされているかを取得する
     *
     * @return bool
     */
    public function getIsFavoritedAttribute()
    {
        if (! auth()->check()) {
            return false;
        }
        return $this->isFavoritedBy(auth()->user());
    }

    /**
     * 指定のユーザからのお気に入りを設定する
     *
     * @param User $user
     * @return void
     */
    public function favoriteBy(User $user)
    {
        if (! $this->isFavoritedBy($user)) {
            return $this->favoriteUsers()->attach($user);
        }
    }

    /**
     * 指定のユーザからのお気に入りを解除する
     *
     * @param User $user
     * @return int
     */
    public function unfavoriteBy(User $user)
    {
        return $this->favoriteUsers()->detach($user);
    }

    /**
     * 指定のユーザからお気に入りにされているか判定する
     *
     * @param User $user
     * @return bool
     */
    public function isFavoritedBy(User $user)
    {
        if ($this->relationLoaded('favoriteUsers')) {
            return $this->favoriteUsers->where('id', $user->id)->isNotEmpty();
        }
        return !! $this->favoriteUsers()->where('user_id', $user->id)->count();
    }
}
