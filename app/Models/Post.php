<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Filters\Filterable;

class Post extends Model
{
    use Favoritable, Filterable;

    /**
     * 複数代入する属性
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'slug',
        'title',
        'content'
    ];

    /**
     * 投稿が属するカテゴリーを取得する
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * 投稿者を取得する
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 投稿に付けられたタグを取得する
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * 投稿に対するコメントを取得する
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * 指定のユーザによって作成されたか判定する
     *
     * @param User $user
     * @return bool
     */
    public function isCreatedBy(User $user)
    {
        return $this->user_id === $user->id;
    }
}
