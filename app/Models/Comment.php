<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * 複数代入する属性
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'content'
    ];

    /**
     * コメントをしたユーザを取得する
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * コメントが属する投稿を取得する
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * 指定のユーザによって作成されたか判定する
     *
     * @param User $user
     * @return bool
     */
    public function isCreatedBy(User $user)
    {
        return $this->user_id === $user->id;
    }
}
