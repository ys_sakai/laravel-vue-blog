<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function () {
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('refresh', 'Auth\RefreshTokenController@refresh');

    Route::get('users/{username}', 'UserController@show');
    Route::get('users/{username}/posts/{slug}', 'PostController@showByKeys');

    Route::resource('posts', 'PostController', ['only' => [
        'index', 'show'
    ]]);

    Route::resource('categories', 'CategoryController', ['only' => [
        'index', 'show'
    ]]);

    Route::resource('tags', 'TagController', ['only' => [
        'index', 'show'
    ]]);

    Route::resource('comments', 'CommentController', ['only' => [
        'index', 'show'
    ]]);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', 'Auth\LoginController@logout');
        Route::get('me', 'MeController@show');
        Route::put('me/password', 'Auth\ChangePasswordController@update');
        Route::put('me/profile', 'MeController@update');

        Route::resource('posts', 'PostController', ['only' => [
            'store', 'update', 'destroy'
        ]]);
        Route::post('posts/{post}/favorite', 'FavoriteController@store');
        Route::delete('posts/{post}/favorite', 'FavoriteController@destroy');

        Route::resource('comments', 'CommentController', ['only' => [
            'store', 'update', 'destroy'
        ]]);

        Route::group(['middleware' => 'can:be-admin'], function () {
            Route::resource('categories', 'CategoryController', ['only' => [
                'store', 'update', 'destroy'
            ]]);

            Route::resource('tags', 'TagController', ['only' => [
                'store', 'update', 'destroy'
            ]]);
        });
    });
});
