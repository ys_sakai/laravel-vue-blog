<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdiminUser();

        factory(App\Models\User::class, 50)->create();
    }

    private function createAdiminUser()
    {
        factory(App\Models\User::class, 1)->create([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'is_admin' => true,
            'bio' => null
        ]);
    }
}
