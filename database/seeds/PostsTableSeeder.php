<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Post::class, 500)
            ->create()
            ->each(function ($p) {
                $n = rand(0, 5);
                if ($n !== 0) {
                    $ids = App\Models\Tag::all()->random($n)->pluck('id');
                    $p->tags()->sync($ids);
                }
            });
    }
}
