<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Favorite::class, function (Faker $faker) {
    return [
        'user_id' => App\Models\User::all()->random()->id,
        'post_id' => App\Models\Post::all()->random()->id
    ];
});
