<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    $number = $faker->unique()->numerify('###');
    return [
        'slug' => 'category-'.$number,
        'name' => 'カテゴリー'.$number
    ];
});
