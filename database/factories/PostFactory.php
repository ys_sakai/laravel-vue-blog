<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'category_id' => App\Models\Category::all()->random()->id,
        'user_id' => App\Models\User::all()->random()->id,
        'slug' => $faker->unique()->lexify('slug-??????????'),
        'title' => $faker->realText(10),
        'content' => $faker->realText(400)."\n\n".$faker->realText(400)."\n\n".$faker->realText(400)
    ];
});
