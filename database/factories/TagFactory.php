<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Tag::class, function (Faker $faker) {
    $number = $faker->unique()->numerify('####');
    return [
        'slug' => 'tag-'.$number,
        'name' => 'タグ'.$number
    ];
});
