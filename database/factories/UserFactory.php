<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->numerify('user_####'),
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('password'),
        'is_admin' => false,
        'bio' => $faker->realText(100),
    ];
});
