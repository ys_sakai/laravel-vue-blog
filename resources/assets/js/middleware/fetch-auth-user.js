import store from '../stores/store';

/**
 * 認証済みの場合にログインユーザを取得する
 *
 * @param {Object} to
 * @param {Object} from
 * @param {Function} next
 */
export default async (to, from, next) => {
  if (store.getters['auth/token'] && !store.getters['auth/user']) {
    await store.dispatch('auth/fetchUser');
  }
  next();
};
