import store from '../stores/store';

/**
 * ルートを管理者に制限する
 *
 * @param {Object} to
 * @param {Object} from
 * @param {Function} next
 */
export default (to, from, next) => {
  if (!store.getters['auth/isAdmin']) {
    next({ path: '/' });
  } else {
    next();
  }
};
