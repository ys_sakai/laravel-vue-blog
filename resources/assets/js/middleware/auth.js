import store from '../stores/store';

/**
 * ルートをログインユーザに制限する
 *
 * @param {Object} to
 * @param {Object} from
 * @param {Function} next
 */
export default (to, from, next) => {
  if (!store.getters['auth/authenticated']) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    });
  } else {
    next();
  }
};
