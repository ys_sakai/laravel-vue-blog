import axios from 'axios';
import store from './stores/store';
import ApiError from './errors/ApiError';
import UnauthorizedError from './errors/UnauthorizedError';
import UnprocessableEntityError from './errors/UnprocessableEntityError';

class Http {
  /**
   * GETメソッドでリクエストする
   *
   * @param {string} url
   * @param {Object} [config]
   * @returns {Promise.<Object|Error>}
   */
  get(url, config) {
    return axios.get(url, config);
  }

  /**
   * POSTメソッドでリクエストする
   *
   * @param {string} url
   * @param {Object} data
   * @param {Object} [config]
   * @returns {Promise.<Object|Error>}
   */
  post(url, data, config) {
    return axios.post(url, data, config);
  }

  /**
   * PUTメソッドでリクエストする
   *
   * @param {string} url
   * @param {Object} data
   * @param {Object} [config]
   * @returns {Promise.<Object|Error>}
   */
  put(url, data, config) {
    return axios.put(url, data, config);
  }

  /**
   * DELETEメソッドでリクエストする
   *
   * @param {string} url
   * @param {Object} [config]
   * @returns {Promise.<Object|Error>}
   */
  delete(url, config) {
    return axios.delete(url, config);
  }

  /**
   * 初期化する
   */
  init() {
    axios.defaults.baseURL = `${window.origin}/api`;

    axios.interceptors.request.use(config => this.modifyRequestConfig(config));

    axios.interceptors.response.use(
      response => this.handleResponse(response),
      error => this.handleError(error)
    );
  }

  /**
   * リクエスト設定に修正を加える
   *
   * @param {Object} config
   * @returns {Object}
   */
  modifyRequestConfig(config) {
    const token = store.getters['auth/token'];

    if (token) {
      config.headers.common.Authorization = `Bearer ${token}`;

      if (config.headers.Authorization) {
        config.headers.Authorization = `Bearer ${token}`;
      }
    }

    return config;
  }

  /**
   * 正常なレスポンスを処理する
   *
   * @param {Object} response
   * @returns {Object}
   */
  handleResponse(response) {
    return response;
  }

  /**
   * エラーを処理する
   *
   * @param {Error} error
   * @returns {Promise.<Object|Error>}
   */
  async handleError(error) {
    console.log(error);

    if (error.response) {
      return this.handleApiError(new ApiError(error));
    }
    return Promise.reject(error);
  }

  /**
   * APIエラーを処理する
   *
   * @param {ApiError} error
   * @returns {Promise.<Object|Error>}
   */
  async handleApiError(error) {
    switch (error.status) {
      case ApiError.HTTP_UNAUTHORIZED:
        return this.handleUnauthorizedError(new UnauthorizedError(error));
      case ApiError.HTTP_UNPROCESSABLE_ENTITY:
        return Promise.reject(new UnprocessableEntityError(error));
      default:
        return Promise.reject(error);
    }
  }

  /**
   * 未認証エラーを処理する
   *
   * @param {UnauthorizedError} error
   * @returns {Promise.<Object|Error>}
   */
  async handleUnauthorizedError(error) {
    if (this.isTokenRefreshError(error)) {
      return Promise.reject(error);
    }

    await this.refreshToken();
    return axios.request(error.config);
  }

  /**
   * トークンを更新する
   */
  async refreshToken() {
    await store.dispatch('auth/refresh');
  }

  /**
   * トークン更新APIのエラーであるか判定する
   *
   * @param {ApiError} error
   * @returns {boolean}
   */
  isTokenRefreshError(error) {
    return `${axios.defaults.baseURL}/refresh` === error.config.url;
  }
}

export default new Http();
