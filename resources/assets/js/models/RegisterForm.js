/**
 * 新規登録フォーム
 */
export default class RegisterForm {
  /**
   * @param {Object} input
   */
  constructor(input) {
    this.name = input.name;
    this.email = input.email;
    this.password = input.password;
    this.password_confirmation = input.password_confirmation;
  }
}
