import { cloneDeep } from 'lodash';

/**
 * カテゴリー
 */
export default class Category {
  /**
   * @param {Object} record
   */
  constructor(record) {
    this.id = record.id;
    this.slug = record.slug;
    this.name = record.name;
    this.created_at = record.created_at;
    this.updated_at = record.updated_at;

    this.posts_count = record.posts_count;
  }

  /**
   * URL
   *
   * @type {string}
   */
  get url() {
    return `/categories/${this.slug}`;
  }

  /**
   * 自身のディープコピーを生成する
   *
   * @returns {Category}
   */
  clone() {
    return cloneDeep(this);
  }
}
