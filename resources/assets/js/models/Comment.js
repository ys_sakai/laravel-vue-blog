import { cloneDeep } from 'lodash';
import moment from 'moment';
import marked from 'marked';
import User from './User';

/**
 * コメント
 */
export default class Comment {
  constructor(record) {
    this.id = record.id;
    this.user_id = record.user_id;
    this.post_id = record.post_id;
    this.content = record.content;
    this.created_at = record.created_at;
    this.updated_at = record.updated_at;

    this.user = new User(record.user || {});
  }

  /**
   * 更新日時
   *
   * @type {string}
   */
  get updatedAt() {
    if (!this.updated_at) return '';
    return moment(this.updated_at).format('YYYY/MM/DD HH:mm');
  }

  /**
   * マークダウン
   *
   * @type {string}
   */
  get markdown() {
    return this.content ? marked(this.content) : '';
  }

  /**
   * 自身のディープコピーを生成する
   *
   * @returns {Category}
   */
  clone() {
    return cloneDeep(this);
  }
}
