/**
 * 投稿一覧取得パラメータ
 */
export default class PostSearchParams {
  /**
   * @param {Object} params
   */
  constructor(params) {
    this.username = params.username;
    this.category_slug = params.category_slug;
    this.tag_slug = params.tag_slug;
    this.favorite_username = params.favorite_username;
    this.page = params.page ? Number(params.page) : 1;
    this.per_page = params.per_page;
    this.sort = params.sort;
  }

  /**
   * 投稿一覧取得パラメータ（作成時間の降順）を生成する
   *
   * @param {Object} params
   * @returns {PostSearchParams}
   */
  static latest(params) {
    return new this({ ...params, sort: 'latest' });
  }

  /**
   * 投稿一覧取得パラメータ（お気に入りの作成時間の降順）を生成する
   *
   * @param {Object} params
   * @returns {PostSearchParams}
   */
  static favoritesLatest(params) {
    return new this({ ...params, sort: 'favorites_latest' });
  }
}
