/**
 * パスワード変更フォーム
 */
export default class ChnagePasswordForm {
  /**
   * @param {Object} input
   */
  constructor(input) {
    this.password = input.password;
    this.new_password = input.password;
    this.new_password_confirmation = input.password_confirmation;
  }
}
