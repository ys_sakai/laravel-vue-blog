import { cloneDeep } from 'lodash';

/**
 * ユーザ
 */
export default class User {
  /**
   * @param {Object} record
   */
  constructor(record) {
    this.id = record.id;
    this.name = record.name;
    this.email = record.email;
    this.bio = record.bio;
    this.is_admin = record.is_admin;
    this.created_at = record.created_at;
    this.updated_at = record.updated_at;
  }

  /**
   * URL
   *
   * @type {string}
   */
  get url() {
    return `/@${this.name}`;
  }

  /**
   * 管理者であるか判定する
   *
   * @returns {boolean}
   */
  isAdmin() {
    return this.is_admin;
  }

  /**
   * 自身のディープコピーを生成する
   *
   * @returns {Category}
   */
  clone() {
    return cloneDeep(this);
  }
}
