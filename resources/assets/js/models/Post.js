import moment from 'moment';
import marked from 'marked';
import Category from './Category';
import User from './User';
import Tag from './Tag';
import Comment from './Comment';

/**
 * 投稿
 */
export default class Post {
  /**
   * @param {Object} record
   */
  constructor(record) {
    this.id = record.id;
    this.category_id = record.category_id;
    this.user_id = record.user_id;
    this.slug = record.slug;
    this.title = record.title;
    this.content = record.content;
    this.created_at = record.created_at;
    this.updated_at = record.updated_at;

    this.category = new Category(record.category || {});
    this.user = new User(record.user || {});

    this.tags = record.tags ? record.tags.map(record => new Tag(record)) : [];
    this.tag_ids = this.tags.map(tag => tag.id);

    this.comments = record.comments ? record.comments.map(record => new Comment(record)) : [];
    this.comments_count = record.comments ? record.comments.length : record.comments_count;

    this.favorite_users_count = record.favorite_users_count;
    this.is_favorited = record.is_favorited;
  }

  /**
   * 抜粋する文字数
   *
   * @type {number}
   */
  static get excerptNumber() { return 100; }

  /**
   * URL
   *
   * @type {string}
   */
  get url() {
    return `/@${this.user.name}/${this.slug}`;
  }

  /**
   * 投稿日時
   *
   * @type {string}
   */
  get publishedAt() {
    if (!this.created_at) return '';
    return moment(this.created_at).format('YYYY/MM/DD HH:mm');
  }

  /**
   * マークダウン
   *
   * @type {string}
   */
  get markdown() {
    return this.content ? marked(this.content) : '';
  }

  /**
   * 本文抜粋
   *
   * @type {string}
   */
  get excerpt() {
    if (!this.content) return '';
    if (this.content.length <= Post.excerptNumber) return this.content;
    return `${this.content.substr(0, Post.excerptNumber)} ...`;
  }

  /**
   * 指定のIDのコメントを保持しているか判定する
   *
   * @param {number} commentId
   * @returns {boolean}
   */
  hasComment(commentId) {
    return this.comments.some(comment => comment.id === commentId);
  }

  /**
   * コメントを設定する
   *
   * @param {Comment} comment
   */
  setComment(comment) {
    const index = this.comments.findIndex(c => c.id === comment.id);
    if (index === -1) {
      this.comments.push(comment);
      this.comments_count += 1;
    } else {
      this.comments.splice(index, 1, comment);
    }
  }

  /**
   * 指定のIDのコメントを削除する
   *
   * @param {number} commentId
   * @memberof Post
   */
  removeComment(commentId) {
    this.comments = this.comments.filter(comment => comment.id !== commentId);
    this.comments_count -= 1;
  }
}
