import http from '../../http';
import User from '../../models/User';

const state = {
  user: new User({})
};

const getters = {
  /**
   * ユーザ
   *
   * @param {Object} state
   * @returns {User}
   */
  user: state => state.user
};

const mutations = {
  setUser(state, user) {
    state.user = user;
  }
};

const actions = {
  /**
   * 指定のユーザ名のユーザを取得する
   *
   * @param {Object} { commit }
   * @param {string} username
   * @returns {Promise}
   */
  async fetchByName({ commit }, username) {
    return http.get(`users/${username}`).then((response) => {
      const user = new User(response.data);
      commit('setUser', user);
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
