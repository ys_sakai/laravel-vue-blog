import http from '../../http';
import Category from '../../models/Category';

const state = {
  categories: []
};

const getters = {
  /**
   * カテゴリー一覧
   *
   * @param {Object} state
   * @returns {Array.<Category>}
   */
  categories: state => state.categories,

  /**
   * 指定のIDのカテゴリーを取得する
   *
   * @param {Object} state
   * @returns {Function}
   */
  findCategoryById: state => (id) => {
    return state.categories.find(category => category.id === id);
  },

  /**
   * 指定のスラッグのカテゴリーを取得する
   *
   * @param {Object} state
   * @returns {Function}
   */
  findCategoryBySlug: state => (slug) => {
    return state.categories.find(category => category.slug === slug);
  }
};

const mutations = {
  setCategories(state, categories) {
    state.categories = categories;
  },

  setCategory(state, { id, category }) {
    const index = state.categories.findIndex(category => category.id === id);
    if (index === -1) {
      state.categories.push(category);
    } else {
      state.categories.splice(index, 1, category);
    }
  },

  removeCategory(state, id) {
    state.categories = state.categories.filter(category => category.id !== id);
  }
};

const actions = {
  /**
   * カテゴリー一覧を取得する
   *
   * @param {Object} { commit }
   * @returns {Promise}
   */
  async fetch({ commit }) {
    return http.get('categories').then((response) => {
      const categories = response.data.map(record => new Category(record));
      commit('setCategories', categories);
    });
  },

  /**
   * 指定のIDのカテゴリーを取得する
   *
   * @param {Object} { commit }
   * @param {number} id
   * @returns {Promise}
   */
  async fetchById({ commit }, id) {
    return http.get(`categories/${id}`).then((response) => {
      const category = new Category(response.data);
      commit('setCategory', { id, category });
    });
  },

  /**
   * カテゴリーを作成する
   *
   * @param {Object} { commit }
   * @param {Category} category
   * @returns {Promise}
   */
  async create({ commit }, category) {
    return http.post('categories', category).then((response) => {
      const category = new Category(response.data);
      commit('setCategory', { id: category.id, category });
    });
  },

  /**
   * カテゴリーを更新する
   *
   * @param {Object} { commit }
   * @param {Object} payload
   * @param {number} payload.id
   * @param {Category} payload.category
   * @returns {Promise}
   */
  async update({ commit }, { id, category }) {
    return http.put(`categories/${id}`, category).then((response) => {
      const category = new Category(response.data);
      commit('setCategory', { id, category });
    });
  },

  /**
   * カテゴリーを削除する
   *
   * @param {Object} { commit }
   * @param {number} id
   * @returns {Promise}
   */
  async delete({ commit }, id) {
    return http.delete(`categories/${id}`).then((response) => {
      commit('removeCategory', id);
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
