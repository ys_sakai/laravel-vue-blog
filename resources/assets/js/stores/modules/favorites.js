import http from '../../http';
import Post from '../../models/Post';

const state = {};

const getters = {};

const mutations = {};

const actions = {
  /**
   * 投稿をお気に入りに設定する
   *
   * @param {Object} { commit }
   * @param {number} id
   * @returns {Promise}
   */
  async favorite({ commit }, id) {
    return http.post(`posts/${id}/favorite`).then((response) => {
      const post = new Post(response.data);
      commit('posts/setPost', { id, post }, { root: true });
    });
  },

  /**
   * 投稿をお気に入りから解除する
   *
   * @param {Object} { commit }
   * @param {number} id
   * @returns {Promise}
   */
  async unfavorite({ commit }, id) {
    return http.delete(`posts/${id}/favorite`).then((response) => {
      const post = new Post(response.data);
      commit('posts/setPost', { id, post }, { root: true });
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
