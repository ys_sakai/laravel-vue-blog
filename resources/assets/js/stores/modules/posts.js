import http from '../../http';
import Post from '../../models/Post';
import PostSearchParams from '../../models/PostSearchParams';

const state = {
  posts: [],
  total: 0,
  page: {
    current: 0,
    last: 0
  }
};

const getters = {
  /**
   * 投稿一覧
   *
   * @param {Object} state
   * @returns {Array.<Post>}
   */
  posts: state => state.posts,

  /**
   * 件数
   *
   * @param {Object} state
   * @returns {number}
   */
  total: state => state.total,

  /**
   * ページ
   *
   * @param {Object} state
   * @returns {{ current: number, last: number }}
   */
  page: state => state.page,

  /**
   * 指定のIDの投稿を取得する
   *
   * @param {Object} state
   * @returns {Function}
   */
  findPostById: state => (id) => {
    return state.posts.find(post => post.id === id);
  },

  /**
   * 指定のユーザ名とスラッグの投稿を取得する
   *
   * @param {Object} state
   * @returns {Function}
   */
  findPostByKeys: state => (username, slug) => {
    return state.posts.find(post => post.user.name === username && post.slug === slug);
  },

  /**
   * 指定のコメントIDのコメントを保持する投稿を取得する
   *
   * @param {Object} state
   * @returns {Function}
   */
  findPostByCommentId: state => (commentId) => {
    return state.posts.find(post => post.hasComment(commentId));
  }
};

const mutations = {
  setPosts(state, {
    posts, total, current, last
  }) {
    state.posts = posts;
    state.total = total;
    state.page.current = current;
    state.page.last = last;
  },

  setPost(state, { id, post }) {
    const index = state.posts.findIndex(post => post.id === id);
    if (index === -1) {
      state.posts.push(post);
    } else {
      state.posts.splice(index, 1, post);
    }
  },

  removePost(state, id) {
    state.posts = state.posts.filter(post => post.id !== id);
  }
};

const actions = {
  /**
   * 投稿一覧を取得する
   *
   * @param {Object} { commit }
   * @param {PostSearchParams} params
   * @returns {Promise}
   */
  async fetch({ commit }, params) {
    return http.get('posts', { params }).then(({ data }) => {
      commit('setPosts', {
        posts: data.data.map(record => new Post(record)),
        total: data.meta.total,
        current: data.meta.current_page,
        last: data.meta.last_page
      });
    });
  },

  /**
   * 指定のIDの投稿を取得する
   *
   * @param {Object} { commit }
   * @param {number} id
   * @returns {Promise}
   */
  async fetchById({ commit }, id) {
    return http.get(`posts/${id}`).then((response) => {
      const post = new Post(response.data);
      commit('setPost', { id: post.id, post });
    });
  },

  /**
   * 指定のユーザ名とスラッグの投稿を取得する
   *
   * @param {Object} { commit }
   * @param {Object} payload
   * @param {string} payload.username
   * @param {string} payload.slug
   * @returns {Promise}
   */
  async fetchByKeys({ commit }, { username, slug }) {
    return http.get(`users/${username}/posts/${slug}`).then((response) => {
      const post = new Post(response.data);
      commit('setPost', { id: post.id, post });
    });
  },

  /**
   * 投稿を作成する
   *
   * @param {Object} { commit }
   * @param {Post} post
   * @returns {Promise}
   */
  async create({ commit }, post) {
    return http.post('posts', post).then((response) => {
      const post = new Post(response.data);
      commit('setPost', { id: post.id, post });
    });
  },

  /**
   * 投稿を更新する
   *
   * @param {Object} { commit }
   * @param {Object} payload
   * @param {number} payload.id
   * @param {Post} payload.post
   * @returns {Promise}
   */
  async update({ commit }, { id, post }) {
    return http.put(`posts/${id}`, post).then((response) => {
      const post = new Post(response.data);
      commit('setPost', { id, post });
    });
  },

  /**
   * 投稿を削除する
   *
   * @param {Object} { commit }
   * @param {number} id
   * @returns {Promise}
   */
  async delete({ commit }, id) {
    return http.delete(`posts/${id}`).then((response) => {
      commit('removePost', id);
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
