import http from '../../http';
import Tag from '../../models/Tag';

const state = {
  tags: []
};

const getters = {
  /**
   * カテゴリー一覧
   *
   * @param {Object} state
   * @returns {Array.<Tag>}
   */
  tags: state => state.tags,

  /**
   * 投稿数の降順にソートしたカテゴリー一覧
   *
   * @param {Object} state
   * @returns {Array.<Tag>}
   */
  popularTags: state => state.tags.sort((a, b) => b.posts_count - a.posts_count),

  /**
   * 指定のIDのタグを取得する
   *
   * @param {Object} state
   * @returns {Function}
   */
  findTagById: state => (id) => {
    return state.tags.find(tag => tag.id === id);
  },

  /**
   * 指定のスラッグのタグを取得する
   *
   * @param {Object} state
   * @returns {Function}
   */
  findTagBySlug: state => (slug) => {
    return state.tags.find(tag => tag.slug === slug);
  }
};

const mutations = {
  setTags(state, tags) {
    state.tags = tags;
  },

  setTag(state, { id, tag }) {
    const index = state.tags.findIndex(tag => tag.id === id);
    if (index === -1) {
      state.tags.push(tag);
    } else {
      state.tags.splice(index, 1, tag);
    }
  },

  removeTag(state, id) {
    state.tags = state.tags.filter(tag => tag.id !== id);
  }
};

const actions = {
  /**
   * タグ一覧を取得する
   *
   * @param {Object} { commit }
   * @returns {Promise}
   */
  async fetch({ commit }) {
    return http.get('tags').then((response) => {
      const tags = response.data.map(record => new Tag(record));
      commit('setTags', tags);
    });
  },

  /**
   * 指定のIDのタグを取得する
   *
   * @param {Object} { commit }
   * @param {number} id
   * @returns {Promise}
   */
  async fetchById({ commit }, id) {
    return http.get(`tags/${id}`).then((response) => {
      const tag = new Tag(response.data);
      commit('setTag', { id, tag });
    });
  },

  /**
   * タグを作成する
   *
   * @param {Object} { commit }
   * @param {Tag} tag
   * @returns {Promise}
   */
  async create({ commit }, tag) {
    return http.post('tags', tag).then((response) => {
      const tag = new Tag(response.data);
      commit('setTag', { id: tag.id, tag });
    });
  },

  /**
   * タグを更新する
   *
   * @param {Object} { commit }
   * @param {Object} payload
   * @param {number} payload.id
   * @param {Tag} payload.tag
   * @returns {Promise}
   */
  async update({ commit }, { id, tag }) {
    return http.put(`tags/${id}`, tag).then((response) => {
      const tag = new Tag(response.data);
      commit('setTag', { id, tag });
    });
  },

  /**
   * タグを削除する
   *
   * @param {Object} { commit }
   * @param {number} id
   * @returns {Promise}
   */
  async delete({ commit }, id) {
    return http.delete(`tags/${id}`).then((response) => {
      commit('removeTag', id);
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
