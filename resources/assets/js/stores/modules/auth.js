import http from '../../http';
import User from '../../models/User';
import RegisterForm from '../../models/RegisterForm';
import ChangePasswordForm from '../../models/ChangePasswordForm';

const state = {
  user: null,
  token: localStorage.getItem('token'),
  tryingRefresh: false,
  refreshCallbacks: []
};

const getters = {
  /**
   * ログインユーザ
   *
   * @param {Object} state
   * @returns {User}
   */
  user: state => state.user,

  /**
   * トークン
   *
   * @param {Object} state
   * @returns {string}
   */
  token: state => state.token,

  /**
   * 認証されているか
   *
   * @param {Object} state
   * @returns {boolean}
   */
  authenticated: state => !!state.user && !!state.token,

  /**
   * 管理者であるか
   *
   * @param {Object} state
   * @returns {boolean}
   */
  isAdmin: state => state.user && state.user.isAdmin(),

  /**
   * トークン更新中であるか
   *
   * @param {Object} state
   * @returns {boolean}
   */
  tryingRefresh: state => state.tryingRefresh
};

const mutations = {
  login(state, token) {
    state.token = token;
    localStorage.setItem('token', token);
  },

  setUser(state, user) {
    state.user = user;
  },

  logout(state) {
    state.user = null;
    state.token = null;
    localStorage.removeItem('token');
  },

  tryRefresh(state, callback) {
    state.tryingRefresh = true;
    state.refreshCallbacks.push(callback);
  },

  refreshSucess(state) {
    const callbacks = state.refreshCallbacks;

    state.tryingRefresh = false;
    state.refreshCallbacks = [];

    callbacks.forEach(callback => callback());
  },

  refreshFailure(state, error) {
    const callbacks = state.refreshCallbacks;

    state.tryingRefresh = false;
    state.refreshCallbacks = [];

    callbacks.forEach(callback => callback(error));
  }
};

const actions = {
  /**
   * ユーザを登録する
   *
   * @param {Object} { commit }
   * @param {RegisterForm} form
   * @returns {Promise}
   */
  async register({ commit }, form) {
    return http.post('register', form).then((response) => {
      commit('login', response.data.access_token);
      commit('setUser', new User(response.data.user));
    });
  },

  /**
   * ログインする
   *
   * @param {Object} { commit }
   * @param {Object} payload
   * @param {string} payload.name
   * @param {string} payload.password
   * @returns {Promise}
   */
  async login({ commit }, { name, password }) {
    return http.post('login', { name, password }).then((response) => {
      commit('login', response.data.access_token);
      commit('setUser', new User(response.data.user));
    });
  },

  /**
   * ログアウトする
   *
   * @param {Object} { commit }
   * @returns {Promise}
   */
  async logout({ commit }) {
    return http.post('logout').then((response) => {
      commit('logout');
    });
  },

  /**
   * トークンを更新する
   *
   * @param {Object} { commit, getters }
   * @returns {Promise}
   */
  async refresh({ commit, getters }) {
    return new Promise(async (resolve, reject) => {
      const isTryingRefresh = getters.tryingRefresh;

      commit('tryRefresh', error => (error ? reject(error) : resolve()));

      if (!isTryingRefresh) {
        await http.post('refresh').then((response) => {
          commit('login', response.data.access_token);
          commit('refreshSucess');
        }).catch((error) => {
          commit('logout');
          commit('refreshFailure', error);
        });
      }
    });
  },

  /**
   * ログインユーザを取得する
   *
   * @param {Object} { commit }
   * @returns {Promise}
   */
  async fetchUser({ commit }) {
    return http.get('me').then((response) => {
      commit('setUser', new User(response.data));
    });
  },

  /**
   * パスワードを変更する
   *
   * @param {Object} { commit }
   * @param {ChangePasswordForm} form
   * @returns {Promise}
   */
  async changePassword({ commit }, form) {
    return http.put('me/password', form);
  },

  /**
   * プロフィールを変更する
   *
   * @param {Object} { commit }
   * @param {User} user
   * @returns {Promise}
   */
  async changeProfile({ commit }, user) {
    return http.put('me/profile', user).then((response) => {
      commit('setUser', new User(response.data));
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
