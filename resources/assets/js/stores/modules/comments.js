import http from '../../http';
import Comment from '../../models/Comment';

const state = {
};

const getters = {
};

const mutations = {
};

const actions = {
  /**
   * コメントを作成する
   *
   * @param {Object} { dispatch }
   * @param {Comment} comment
   * @returns {Promise}
   */
  async create({ dispatch }, comment) {
    return http.post('comments', comment).then((response) => {
      const comment = new Comment(response.data);
      dispatch('setCommentToPost', comment);
    });
  },

  /**
   * コメントを更新する
   *
   * @param {Object} { dispatch }
   * @param {Object} payload
   * @param {number} payload.id
   * @param {Comment} payload.comment
   * @returns {Promise}
   */
  async update({ dispatch }, { id, comment }) {
    return http.put(`comments/${id}`, comment).then((response) => {
      const comment = new Comment(response.data);
      dispatch('setCommentToPost', comment);
    });
  },

  /**
   * コメントを削除する
   *
   * @param {Object} { dispatch }
   * @param {number} id
   * @returns {Promise}
   */
  async delete({ dispatch }, id) {
    return http.delete(`comments/${id}`).then(() => {
      dispatch('removeCommentFromPost', id);
    });
  },

  /**
   * コメントを投稿に設定する
   *
   * @param {Object} { commit, rootGetters }
   * @param {Comment} comment
   */
  setCommentToPost({ commit, rootGetters }, comment) {
    const post = rootGetters['posts/findPostById'](comment.post_id);
    if (post) {
      post.setComment(comment);
      commit('posts/setPost', { id: post.id, post }, { root: true });
    }
  },

  /**
   * コメントを投稿から削除する
   *
   * @param {Object} { commit, rootGetters }
   * @param {number} id
   */
  removeCommentFromPost({ commit, rootGetters }, id) {
    const post = rootGetters['posts/findPostByCommentId'](id);
    if (post) {
      post.removeComment(id);
      commit('posts/setPost', { id: post.id, post }, { root: true });
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
