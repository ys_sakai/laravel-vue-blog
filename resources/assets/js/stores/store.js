import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import users from './modules/users';
import posts from './modules/posts';
import categories from './modules/categories';
import tags from './modules/tags';
import comments from './modules/comments';
import favorites from './modules/favorites';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    users,
    posts,
    categories,
    tags,
    comments,
    favorites
  }
});

export default store;
