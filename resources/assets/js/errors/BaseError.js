/**
 * 独自エラー型の基底クラス
 */
export default class BaseError extends Error {
  /**
   * @param {string} message
   */
  constructor(message) {
    super(message);

    this.name = this.constructor.name;

    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = (new Error(message)).stack;
    }
  }
}
