import ApiError from './ApiError';

export default class UnauthorizedError extends ApiError {}
