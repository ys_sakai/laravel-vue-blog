import BaseError from './BaseError';

/**
 * APIエラー
 */
export default class ApiError extends BaseError {
  /**
   * @param {Error} cause
   */
  constructor(cause) {
    super(cause.message);
    this.cause = cause;
  }

  /** HTTPステータスコード 401 Unauthorized @type number */
  static get HTTP_UNAUTHORIZED() { return 401; }
  /** HTTPステータスコード 422 Unprocessable Entity @type number */
  static get HTTP_UNPROCESSABLE_ENTITY() { return 422; }

  /**
   * axios(https://github.com/axios/axios)のResponse Schema
   *
   * @type {Object}
   */
  get response() {
    return this.cause.response;
  }

  /**
   * axios(https://github.com/axios/axios)のconfig
   *
   * @type {Object}
   */
  get config() {
    return this.cause.config;
  }

  /**
   * HTTPステータスコード
   *
   * @type {number}
   */
  get status() {
    return this.response.status;
  }

  /**
   * レスポンスのデータ
   *
   * @type {Object}
   */
  get data() {
    return this.response.data;
  }
}
