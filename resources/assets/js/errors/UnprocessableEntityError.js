import ApiError from './ApiError';

/**
 * Unprocessable Entityエラー
 */
export default class UnprocessableEntityError extends ApiError {
  /**
   * エラー内容を取得する
   *
   * @returns {Object}
   */
  errors() {
    return this.data.errors;
  }
}
