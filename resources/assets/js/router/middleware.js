import fetchAuthUser from '../middleware/fetch-auth-user';
import auth from '../middleware/auth';
import admin from '../middleware/admin';

/**
 * グローバルのミドルウェア
 *
 * @type {Array.<Function>}
 */
export const globalMiddleware = [
  fetchAuthUser
];

/**
 * ルート別に指定するミドルウェア
 *
 * ルート定義のmeta.middlewareフィールドに、ミドルウェアのキーまたはキーの配列を設定することで指定する。
 *
 * @example
 * {
 *   path: '/foo',
 *   component: Foo,
 *   meta: { middleware: ['auth', 'admin'] }
 * }
 * @type {Object}
 */
export const routeMiddleware = {
  auth,
  admin
};
