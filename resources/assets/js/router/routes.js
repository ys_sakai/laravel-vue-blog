import Home from '../pages/Home.vue';
import Register from '../pages/Register.vue';
import Login from '../pages/Login.vue';
import Profile from '../pages/settings/Profile.vue';
import Password from '../pages/settings/Password.vue';
import Posts from '../pages/Posts.vue';
import Categories from '../pages/Categories.vue';
import Category from '../pages/Category.vue';
import Tags from '../pages/Tags.vue';
import Tag from '../pages/Tag.vue';
import Favorites from '../pages/Favorites.vue';
import User from '../pages/User.vue';
import Post from '../pages/Post.vue';
import EditPostList from '../pages/edit/PostList.vue';
import EditPost from '../pages/edit/Post.vue';
import Admin from '../pages/admin/Admin.vue';
import AdminCategory from '../pages/admin/Category.vue';
import AdminTag from '../pages/admin/Tag.vue';

import PostSearchParams from '../models/PostSearchParams';

export default [
  {
    path: '/',
    component: Home
  },
  {
    path: '/register',
    component: Register,
    props: route => ({ redirect: route.query.redirect })
  },
  {
    path: '/login',
    component: Login,
    props: route => ({ redirect: route.query.redirect })
  },
  {
    path: '/settings/profile',
    component: Profile
  },
  {
    path: '/settings/password',
    component: Password
  },
  {
    path: '/categories',
    component: Categories
  },
  {
    path: '/categories/:slug',
    component: Category,
    props: true,
    children: [
      {
        path: '',
        redirect: 'trend'
      },
      {
        path: 'trend',
        component: Posts,
        props: route => ({
          params: PostSearchParams.favoritesLatest({
            category_slug: route.params.slug,
            page: route.query.page
          })
        })
      },
      {
        path: 'new',
        component: Posts,
        props: route => ({
          params: PostSearchParams.latest({
            category_slug: route.params.slug,
            page: route.query.page
          })
        })
      }
    ]
  },
  {
    path: '/tags',
    component: Tags
  },
  {
    path: '/tags/:slug',
    component: Tag,
    props: true,
    children: [
      {
        path: '',
        redirect: 'trend'
      },
      {
        path: 'trend',
        component: Posts,
        props: route => ({
          params: PostSearchParams.favoritesLatest({
            tag_slug: route.params.slug,
            page: route.query.page
          })
        })
      },
      {
        path: 'new',
        component: Posts,
        props: route => ({
          params: PostSearchParams.latest({
            tag_slug: route.params.slug,
            page: route.query.page
          })
        })
      }
    ]
  },
  {
    path: '/favorites',
    component: Favorites,
    props: route => ({ page: route.query.page ? Number(route.query.page) : 1 })
  },
  {
    path: '/@:username',
    component: User,
    props: true,
    children: [
      {
        path: '',
        redirect: 'posts'
      },
      {
        path: 'posts',
        component: Posts,
        props: route => ({
          params: PostSearchParams.latest({
            username: route.params.username,
            page: route.query.page
          })
        })
      },
      {
        path: 'favorites',
        component: Posts,
        props: route => ({
          params: PostSearchParams.favoritesLatest({
            favorite_username: route.params.username,
            page: route.query.page
          })
        })
      }
    ]
  },
  {
    path: '/@:username/:slug',
    component: Post,
    props: true
  },
  {
    path: '/edit/posts',
    component: EditPostList,
    meta: { middleware: 'auth' },
    props: route => ({ page: route.query.page ? Number(route.query.page) : 1 })
  },
  {
    path: '/edit/posts/new',
    component: EditPost,
    meta: { middleware: 'auth' }
  },
  {
    path: '/edit/posts/:id',
    component: EditPost,
    meta: { middleware: 'auth' },
    props: route => ({ id: Number(route.params.id) })
  },
  {
    path: '/admin',
    component: Admin,
    meta: { middleware: ['auth', 'admin'] },
    children: [
      {
        path: '',
        redirect: 'categories'
      },
      {
        path: 'categories',
        component: AdminCategory
      },
      {
        path: 'tags',
        component: AdminTag
      }
    ]
  }
];
