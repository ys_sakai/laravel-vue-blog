import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import { globalMiddleware, routeMiddleware } from './middleware';

Vue.use(VueRouter);

class Router {
  /**
   * VueRouterインスタンスを生成する
   *
   * @returns {VueRouter}
   */
  static createVueRouter() {
    const router = new this();

    const vueRouter = new VueRouter({
      mode: 'history',
      routes,
      linkActiveClass: 'active',
      scrollBehavior: (...args) => router.handleScrollBehavior(...args)
    });

    vueRouter.beforeEach((...args) => router.handleBeforeEach(...args));

    return vueRouter;
  }

  /**
   * スクロールのふるまいを制御する
   *
   * @param {Object} to
   * @param {Object} from
   * @param {Object} savedPosition
   * @returns {Object}
   */
  handleScrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return document.querySelector(to.hash) ? { selector: to.hash } : false;
    }
    return { x: 0, y: 0 };
  }

  /**
   * グローバルbeforeガードの処理を行う
   *
   * @param {Object} to
   * @param {Object} from
   * @param {Function} next
   */
  handleBeforeEach(to, from, next) {
    const middlewares = this.getMiddlewares(to);
    this.callMiddlewares(middlewares, to, from, next);
  }

  /**
   * 各ミドルウェアを呼び出す
   *
   * @param {Array.<Function|string>} middlewares
   * @param {Object} to
   * @param {Object} from
   * @param {Function} next
   */
  callMiddlewares(middlewares, to, from, next) {
    const stack = middlewares.reverse();

    const nextCallback = (...args) => {
      if (args.length > 0 || stack.length === 0) {
        next(...args);
        return;
      }

      const middleware = stack.pop();

      if (typeof middleware === 'function') {
        middleware(to, from, nextCallback);
      } else if (routeMiddleware[middleware]) {
        routeMiddleware[middleware](to, from, nextCallback);
      } else {
        throw Error(`Undefined middleware [${middleware}]`);
      }
    };

    nextCallback();
  }

  /**
   * 指定のルートで使用する各ミドルウェアを取得する
   *
   * @param {Object} to
   * @returns {Array.<Function|string>}
   */
  getMiddlewares(to) {
    const middlewares = [...globalMiddleware];

    to.matched.filter(record => record.meta && record.meta.middleware)
      .forEach((record) => {
        if (Array.isArray(record.meta.middleware)) {
          middlewares.push(...record.meta.middleware);
        } else {
          middlewares.push(record.meta.middleware);
        }
      });

    return middlewares;
  }
}

export default Router.createVueRouter();
