import Vue from 'vue';
import './bootstrap';
import App from './App.vue';
import router from './router/router';
import store from './stores/store';
import http from './http';

http.init();

const app = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
});
